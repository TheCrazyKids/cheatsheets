# AC Cheatsheet
If you're looking at this sheet it's probably because you're not familiar with the AC system. Which is fine, there's a lot of shit you need to know and probably not a lot of care to learn every bit of a gmod server; this document will least through pretty much everything you need to know as to not make fuck ups.

___
## Table of Contents
  - [OD, a poor mans powertrip](#od-a-poor-mans-powertrip)
  - [Why we don't use objectives](#why-we-dont-use-objectives)
  - [Statuses, now we're talking](#statuses-now-were-talking)
  - [Why RC are our friends ^_^](#why-rc-are-our-friends-_)
  - [Clearance Levels (Dang, I guess my worth is measured in numbers after all)](#clearance-levels-dang-i-guess-my-worth-is-measured-in-numbers-after-all)
  - [ATC, and why is someone else approving flight requests?](#atc-and-why-is-someone-else-approving-flight-requests)
  - [Learn this simple trick that will make Commanders hate you, AC Assignments](#learn-this-simple-trick-that-will-make-commanders-hate-you-ac-assignments)
  - [Alice: Yes, Sky made his waifu into Garry's Mod](#alice-yes-sky-made-his-waifu-into-garrys-mod)
  - [Your personal dominatrix Honour Guard](#your-personal-dominatrix-honour-guard)
  - [How to complain 101](#how-to-complain-101)
___
## OD, a poor mans powertrip
The OD system is *extremely* simple, an AC OD a person who has essentially taken control of the base or an event and can order people to do whatever the fuck they want.

The current OD is the top most rank, not even AC IC can trump an OD command. You're expected to use this power wisely however, use your power only for the benefit of the players and not to minge. Remember, with great power comes great chances of an AC IC ordering CG to detain you and ruin your entire career.

There should always be an OD on at all times, and *especially* during events. 
>You can toggle OD by using the `/od` command, it can only be used by clearance level 4 and above.
___
## Why we don't use objectives
Objectives is a kinda neat feature that no-one uses, it's essentially a rebrand of the good ol' DarkRP agenda system. You can use the `/agenda` command to set the `Current Objective` on the top right hand of the screen.

Unfortunately it's very rarely used and every more rarely looked at, but you can feel free to try and break the norms and get people to use it anyway. God knows I've given up.
___
## Statuses, now we're talking
The "Premium" objective system, with this you can quickly broadcast preset commands across the entire base, there's not much to say here. If you've played on the server for a base attack you've seen this thing in action. It's the announcement box in the top right hand corner.

You can switch statuses using the `!status` command, it can only be used by clearance level 4 and above. You can see the full list of statuses using the `!status help` command.
> Statuses cannot be used on maps that are not the base map, it's just not how the world works.
___
## Why RC are our friends ^_^
RC have a good reputation around gmod of being elitists, and most of the time not rightfully so. Luckily on Eternal RC are treated more like actual people rather than gods to be revered.

- AC are FULLY permitted to order RC around, as long as it doesn't go against orders they may have received from a higher rank.
> Hierachy of orders from lowest to highest in priority: AC -> AC Assigned/IC -> AC OD -> Event Organiser
- RC are NOT exempt from server rules, and are held accountable for their own actions (unless they were ordered to do their action, then whoever ordered em is responsible for their actions).
- RC are NOT permitted to go anywhere and do anything they want, pretty simple. They run along the same clearance levels as we'll cover in the next section.
___
## Clearance Levels (Dang, I guess my worth is measured in numbers after all)
Clearance Levels determine where people can and can't go, along with what they can and can't do. Here are a brief go over of the clearance levels:
1. Trooper Level Clearance: literally just a "i'm part of the republic!" ID card and will allow you to keep walking around when a CG decides they want to gauge your ability to keep on existing.
2. Non-Commissioned Officer Clearance
3. Commissioned Officer Clearance
4. Commanding Officer Clearance, here's where things get interesting. Commanding Officers have access to most non-AC/RC areas on the base along with access to the OD, objective and some other neat features.
5. AC Clearance: AC have access to everything and everywhere. Not much to say here.
> Random other clearances:
>
> Omega Clearance: RC Only, marks an AC only area or permission.
___
## ATC, and why is someone else approving flight requests?
ATC is the control point between command and pilots, and has two seperate functions.

The first function is the Auto-ATC, usually the ATC is set up to automatically grant and deny takeoff and landing requests. When taking off the system makes sure that the airway is safe before granting the takeoff, and when landing the system assigns a safe landing location.
> You can disable Auto-ATC to restore manual control by using `/baseai System Call, Disable Auto-ATC` and you can re-enable it by substituting `Disable` for `Enable`.

The second function is the ATC chat channel, this is where pilots can talk to you and you can talk to pilots. Pretty simply stuff.

Last but not least is the good old scramble order, scramble order is an order that tells all available pilots to get in the air immediately. Takeoff requests are no longer required in a scramble order (although landing requests are).
___
## Learn this simple trick that will make Commanders hate you, AC Assignments
AC Assignments are something AC get which will assign them for a regiment. Pretty simple right? Essentially what an AC Assigned is is to be an advisor for a commander and also a contact point between AC and a regiment. Their job is to keep an eye on a regiment and to make adjustments when necessary to make sure the regiment keeps performing.

AC Assigned do NOT have any permissions over the regiment besides what the commander gives them, if they have proper reason to make a change they can request permission from AC IC in their assignment channel who will review the situation and grant permission if necessary.
___
## Alice: Yes, Sky made his waifu into Garry's Mod
Alice is a BaseAI system, aka there are commands on server that will cause stuff to happen. Stuff like asking Alice to open up the main gate, it's pretty self explanatory.

BaseAI Commands can be found [here](https://gitlab.com/TheCrazyKids/baseai/-/blob/main/guide.md) and can be activated by using the command `/baseai`
___
## Your personal dominatrix Honour Guard
Honour Guard are the Clone Guard troopers who decided they didn't have anything better to do with their time except to play Garry's Mod and stand next to AC. If you're an OD, that's really unfortunate for you cause these guys have full permission to prevent you from going within 10 miles of the battlefield.

If you're a non-OD AC, great! You're still required to have an Honour Guard guard to guard you against the stuff you need to be guarded from, but you are allowed to enter the battlefield and actually play the fucking game.
> Honour Guard have permission to restrain and move an OD that is not following their orders for their safety, this rule does not persist for non-OD AC who can happily take Honour Guard orders under recommendation and promptly bin them.
___
## How to complain 101
So you have a complaint hm? Here are your contacts to direct them to make them someone elses problem:
- Trooper Complaint: Talk to the Commander of the Troop
- Commander Complaint: Talk to the Assigned AC of the Commanders regiment
- AC Complaint: Talk to the AC IC
- AC IC Complaint: Talk to the other AC IC, or Memeti if unavailable
> If you feel your complaint was not handled properly, feel free to take it up with the next person in the hierachy.